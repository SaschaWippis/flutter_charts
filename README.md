# charts

A new Flutter package project.

## Getting Started

#### Add the dependency
Add -TODO IMPLEMENT- to the dependency section of your pubspec.yaml.
```
dependencies:
  -TODO IMPLEMENT-
```

Install the package from the command line
```
flutter packages get
```

### Using Line charts

To add a line chart to your code, simply add the LineChart widget to your code and give it a list of ChartValuePair.
Supported type for the X and Y values of the ChartValuePair are *double*, *int* and *DateTime*  

```dart
final List<ChartValuePair> myChartData = List.generate(
        10,
        (index) => ChartValuePair(
              x: DateTime.now().add(Duration(hours: index)),
              y: index * 10,
            ));

return Container(child: 
            LineChart(
                valuePairs: myChartData
            ),
        );
```