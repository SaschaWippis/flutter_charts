import 'package:charts/utils/range_conversion.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('convertToNewRange correctly converts a value into a new range', () {
    final point = 5.0;
    final oldMin = 5.0;
    final oldMax = 10.0;
    final newMin = 50.0;
    final newMax = 100.0;

    expect(
        convertToNewRange(
            point: point,
            oldMin: oldMin,
            oldMax: oldMax,
            newMin: newMin,
            newMax: newMax),
        50);
  });
}
