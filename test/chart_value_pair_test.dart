import 'package:charts/charts.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('ChartValuePair.getAxis(axis) gets returns the correct value', () {
    final chartValuePair = ChartValuePair(x: 2, y: 5);

    expect(chartValuePair.getAxis(Axis.horizontal), 2);
    expect(chartValuePair.getAxis(Axis.vertical), 5);
  });

  test('ChartValuePair.convertToDouble(value) converts everything into doubles',
      () {
    final String testNumberString = "24";
    final double testDouble = 24;
    final int testInt = 24;
    final DateTime testDateTime = DateTime.now();

    expect(ChartValuePair.convertToDouble(testNumberString) is double, true);
    expect(ChartValuePair.convertToDouble(testDouble) is double, true);
    expect(ChartValuePair.convertToDouble(testInt) is double, true);
    expect(ChartValuePair.convertToDouble(testDateTime) is double, true);
  });

  test('ChartValuePair.convertToOffset(...) returns a correctly scaled offset',
      () {
    final point = ChartValuePair(x: 50, y: 100);
    final minPoint = ChartValuePair(x: 0, y: 0);
    final maxPoint = ChartValuePair(x: 100, y: 200);
    final canvasMinSize = Size(0, 0);
    final canvasSize = Size(200, 400);

    final offset = ChartValuePair.convertToOffset(
      point: point,
      minPoint: minPoint,
      maxPoint: maxPoint,
      newMinSize: canvasMinSize,
      newMaxSize: canvasSize,
    );
    expect(offset is Offset, true);
    expect(offset, Offset(100, 200));
  });
}
