import 'package:charts/charts.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('ChartValues correctly initializes', () {
    final valuePairs = List.generate(10, (index) {
      return ChartValuePair(x: index, y: index * 10);
    });
    final chartValues = ChartValues(valuePairs);

    chartValues.initialize();
    expect(chartValues.minMaxX[0].x, 0);
    expect(chartValues.minMaxX[1].x, 9);

    expect(chartValues.minMaxX[0].y, 0);
    expect(chartValues.minMaxX[1].y, 90);
  });
}
