import 'package:charts/utils/range_conversion.dart';
import 'package:flutter/material.dart';

/// The data object for the visualization of a point in the LineChart.
/// X and Y can be of type *double*, *int* or *DateTime*.
class ChartValuePair<X, Y> {
  final X x;
  final Y y;

  const ChartValuePair({
    this.x,
    this.y,
  });

  @override
  String toString() {
    return "{x: $x, y: $y }";
  }

  dynamic getAxis(Axis axis) {
    return axis == Axis.horizontal ? x : y;
  }

  static double convertToDouble(dynamic value) {
    switch (value.runtimeType) {
      case DateTime:
        return value.millisecondsSinceEpoch.toDouble();
      case String:
        return double.tryParse(value) is double ? double.parse(value) : 0;
      case double:
        return value;
      case int:
      default:
        return value.toDouble();
    }
  }

  static Offset convertToOffset({
    @required ChartValuePair point,
    @required ChartValuePair minPoint,
    @required ChartValuePair maxPoint,
    @required Size newMinSize,
    @required Size newMaxSize,
  }) {
    final x = convertToNewRange(
      point: convertToDouble(point.x),
      oldMin: convertToDouble(minPoint.x),
      oldMax: convertToDouble(maxPoint.x),
      newMin: newMinSize.width,
      newMax: newMaxSize.width,
    );
    final y = convertToNewRange(
      point: convertToDouble(point.y),
      oldMin: convertToDouble(minPoint.y),
      oldMax: convertToDouble(maxPoint.y),
      newMin: newMinSize.height,
      newMax: newMaxSize.height,
    );
    return Offset(x, y);
  }
}
