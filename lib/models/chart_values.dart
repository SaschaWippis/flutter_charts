import 'package:flutter/material.dart';
import 'package:charts/models/chart_value_pair.dart';

class ChartValues {
  final List<ChartValuePair> chartValuePairs;

  ChartValues(this.chartValuePairs);

  List<ChartValuePair> _minMaxX;
  List<ChartValuePair> _minMaxY;

  /// The minimum [0] and maximum [1] valuePairs calculated by the **X** axis.
  List<ChartValuePair> get minMaxX => _minMaxX;

  /// The minimum [0] and maximum [1] valuePairs calculated by the **Y** axis.
  List<ChartValuePair> get minMaxY => _minMaxY;

  void initialize() {
    findMinMax(Axis.horizontal);
    findMinMax(Axis.vertical);
  }

  void findMinMax(Axis axis) {
    ChartValuePair min;
    ChartValuePair max;

    chartValuePairs.forEach((e) {
      if (min == null) {
        min = e;
        max = e;
      } else {
        switch (min.getAxis(axis).runtimeType) {
          case DateTime:
            if (min.getAxis(axis) is DateTime) {
              if (e.getAxis(axis).isBefore(min.getAxis(axis))) {
                min = e;
              }
              if (e.getAxis(axis).isAfter(max.getAxis(axis))) {
                max = e;
              }
            }

            break;
          default:
            if (e.getAxis(axis) < min.getAxis(axis)) {
              min = e;
            }
            if (e.getAxis(axis) > max.getAxis(axis)) {
              max = e;
            }
            break;
        }
      }
    });

    if (axis == Axis.horizontal) {
      _minMaxX = [min, max];
    } else {
      _minMaxY = [min, max];
    }
  }
}
