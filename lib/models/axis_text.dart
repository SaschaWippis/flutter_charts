/// Defines the position and text on the side of an axis
class AxisText {
  /// The position in canvas scales of either the X or Y axis.
  double position;

  /// The text to be diplayed on the axis label.
  String text;

  AxisText({
    this.position,
    this.text,
  });

  @override
  String toString() {
    return "position: $position, text: $text";
  }
}
