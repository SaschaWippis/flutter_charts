class ValueLabelPair {
  final String text;
  final double value;

  ValueLabelPair(
    this.text,
    this.value,
  );
}
