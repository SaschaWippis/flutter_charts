import 'package:charts/utils/min_max.dart';
import 'package:charts/utils/range_conversion.dart';
import 'package:flutter/material.dart';
import 'package:charts/models/value_label_pair.dart';
import 'package:charts/charts/style/chart_border_style.dart';

class BarChart extends StatefulWidget {
  final List<ValueLabelPair> values;
  final bool drawBorder;
  final ChartBorderStyle borderStyle;
  final Axis orientation;
  const BarChart({
    Key key,
    @required this.values,
    this.drawBorder = true,
    this.borderStyle,
    this.orientation = Axis.horizontal,
  }) : super(key: key);
  @override
  _BarChartState createState() => _BarChartState();
}

class _BarChartState extends State<BarChart> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return CustomPaint(
          size: Size(constraints.maxWidth, constraints.maxHeight),
          painter: _BarChartPainter(
            values: widget.values,
            drawBorder: widget.drawBorder,
            chartBorderStyle: widget.borderStyle,
            orientation: widget.orientation,
          ),
        );
      },
    );
  }
}

class _BarChartPainter extends CustomPainter {
  final List<ValueLabelPair> values;
  final bool drawBorder;
  final ChartBorderStyle chartBorderStyle;
  final Axis orientation;

  _BarChartPainter({
    @required this.values,
    @required this.drawBorder,
    @required this.chartBorderStyle,
    @required this.orientation,
  });

  double _xAxisOffset = 30;
  double _yAxisOffset = 20;
  double _axisPadding = 50;
  double _stepWidth;
  double _barMargin = 10;
  List<double> _barHeights = [];
  void _drawBorders(Canvas canvas, Size size, Paint paint) {
    Path path = Path()
      ..moveTo(_xAxisOffset, 0)
      ..lineTo(_xAxisOffset, size.height - _yAxisOffset)
      ..lineTo(size.width, size.height - _yAxisOffset);

    canvas.drawPath(path, paint);
  }

  void _drawBars(Canvas canvas, Size size) {
    if (orientation == Axis.horizontal) {
      _stepWidth = (size.width - _axisPadding) / values.length;
    } else {
      _stepWidth = (size.height - _axisPadding) / values.length;
    }

    final minMax = minMaxed(values.map((e) => e.value).toList());

    for (int index = 0; index < values.length; index++) {
      final height = convertToNewRange(
        point: values[index].value,
        oldMin: 0,
        oldMax: minMax[1],
        newMin: orientation == Axis.horizontal ? 0 : _xAxisOffset,
        newMax: orientation == Axis.horizontal
            ? size.height
            : size.width - _xAxisOffset,
      );
      Rect bar;
      if (orientation == Axis.horizontal) {
        bar = Rect.fromLTWH(
          _axisPadding + (_stepWidth * index),
          size.height - _yAxisOffset,
          _stepWidth - _barMargin,
          -height,
        );
        _barHeights.add(-height);
      } else {
        bar = Rect.fromLTWH(
          _xAxisOffset,
          size.height - (_stepWidth * index) - _axisPadding - _yAxisOffset,
          height,
          _stepWidth - _barMargin,
        );
        _barHeights.add(height);
      }
      canvas.drawRect(
          bar,
          Paint()
            ..color =
                Color.fromARGB((255 / (index + 1)).round(), 255, 255, 255));
    }
  }

  @override
  void paint(Canvas canvas, Size size) {
    values.sort((a, b) => b.value.compareTo(a.value));
    Paint borderPaint = Paint()
      ..color = chartBorderStyle.color
      ..strokeWidth = chartBorderStyle.width
      ..style = PaintingStyle.stroke;
    if (drawBorder) _drawBorders(canvas, size, borderPaint);
    _drawBars(canvas, size);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
