import 'package:charts/utils/date_formatter.dart';
import 'package:charts/utils/label_conversion.dart';
import 'package:charts/utils/range_conversion.dart';
import 'package:flutter/material.dart';
import 'package:charts/models/chart_values.dart';
import 'package:charts/models/chart_value_pair.dart';
import 'package:charts/charts/style/chart_border_style.dart';
import 'package:charts/charts/style/chart_line_style.dart';
import 'package:charts/charts/style/chart_path_style.dart';
import 'package:charts/charts/style/chart_point_style.dart';
import 'package:charts/models/axis_text.dart';
import 'package:charts/charts/line_chart/animated_path_util.dart';

class LineChart extends StatefulWidget {
  /// The data to be visualized.
  final ChartValues chartValues;

  /// Default is *true*
  final bool animate;

  /// Default is 1 second
  final Duration animationDuration;

  /// Basically the amount of labels that will be displayed between the minimun and the maximum **X** values of the [chartValuePairs].
  final int xAxisStepCount;

  /// Basically the amount of labels that will be displayed between the minimun and the maximum **Y** values of the [chartValuePairs].
  final int yAxisStepCount;

  /// If *true*, the chart will draw a path along the X axis.
  /// Default is *true*
  final bool drawPath;

  /// If *true*, circles will be rendered on every point of the [valuePairs]
  /// Default is *false*
  final bool drawDots;

  /// Default is *false*
  final bool drawGrid;

  /// Draw a line across the **Y** axis to show the average **Y** value.
  /// Default is *false*
  final bool showAverageLevel;

  /// Default is *true*
  final bool showLabels;

  /// Default is *true*
  final bool drawBorders;
  final bool drawHighlights;
  final bool useDragInteraction;

  final ChartPathStyle pathStyle;
  final ChartBorderStyle borderStyle;
  final ChartLineStyle gridLineStyle;
  final ChartLineStyle averageLevelLineStyle;
  final ChartPointStyle pointStyle;

  /// The [TextStyle] for the labels displayed on the axes.
  final TextStyle labelStyle;
  final double xOffset;
  final double yOffset;

  LineChart({
    Key key,
    @required this.chartValues,
    this.animate = true,
    this.animationDuration = const Duration(seconds: 1),
    this.xAxisStepCount = 5,
    this.yAxisStepCount = 5,
    this.drawPath = true,
    this.drawDots = false,
    this.drawGrid = false,
    this.showAverageLevel = false,
    this.showLabels = true,
    this.drawBorders = true,
    this.useDragInteraction = false,
    this.pathStyle = const ChartPathStyle(),
    this.borderStyle = const ChartBorderStyle(),
    this.gridLineStyle = const ChartLineStyle(),
    this.averageLevelLineStyle = const ChartLineStyle(),
    this.pointStyle = const ChartPointStyle(),
    this.labelStyle,
    this.drawHighlights = true,
    this.xOffset = 40,
    this.yOffset = 20,
  }) : super(key: key);

  @override
  _LineChartState createState() => _LineChartState();
}

class _LineChartState extends State<LineChart>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  bool _horizontalDragActive = false;
  double _horizontalDragPosition = 0.0;

  @override
  void initState() {
    super.initState();

    _animationController = AnimationController(
      vsync: this,
      duration: widget.animationDuration,
    )
      ..forward()
      ..addListener(() {
        setState(() {});
      });
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget lineChart = LayoutBuilder(
      builder: (context, constraints) {
        return CustomPaint(
          size: Size(constraints.maxWidth, constraints.maxHeight),
          painter: _LineChartPainter(
            animationValue: widget.animate ? _animationController.value : 1,
            chartValues: widget.chartValues,
            xAxisStepCount: widget.xAxisStepCount,
            yAxisStepCount: widget.yAxisStepCount,
            drawDots: widget.drawDots,
            drawBorders: widget.drawBorders,
            drawPath: widget.drawPath,
            drawGrid: widget.drawGrid,
            useDragInteraction: widget.useDragInteraction,
            showAverageLevel: widget.showAverageLevel,
            borderStyle: widget.borderStyle,
            gridLineStyle: widget.gridLineStyle,
            averageLevelLineStyle: widget.averageLevelLineStyle,
            pointStyle: widget.pointStyle,
            showLabels: widget.showLabels,
            pathStyle: widget.pathStyle,
            labelStyle: widget.labelStyle,
            horizontalDragActive: _horizontalDragActive,
            horizontalDragPosition: _horizontalDragPosition,
            drawHighlights: widget.drawHighlights,
            xOffset: widget.xOffset,
            yOffset: widget.yOffset,
          ),
        );
      },
    );

    if (widget.chartValues != null &&
        widget.chartValues.chartValuePairs.isNotEmpty) {
      if (widget.useDragInteraction) {
        return GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTapDown: (tap) {
            _horizontalDragActive = true;
            _horizontalDragPosition = tap.globalPosition.dx;
            setState(() {});
          },
          onHorizontalDragStart: (dragStartDetails) {
            _horizontalDragActive = true;
            _horizontalDragPosition = dragStartDetails.globalPosition.dx;
            setState(() {});
          },
          onHorizontalDragUpdate: (dragUpdateDetails) {
            _horizontalDragPosition += dragUpdateDetails.primaryDelta;
            setState(() {});
          },
          onHorizontalDragEnd: (dragEndDetails) {
            _horizontalDragActive = false;
            _horizontalDragPosition = 0.0;
            setState(() {});
          },
          onTapUp: (tap) {
            _horizontalDragActive = false;
            _horizontalDragPosition = 0.0;
            setState(() {});
          },
          child: lineChart,
        );
      } else {
        return lineChart;
      }
    } else {
      return Container();
    }
  }
}

class _LineChartPainter extends CustomPainter {
  final ChartValues chartValues;
  final double animationValue;

  /// Basically the amount of labels that will be displayed between the minimun and the maximum **X** values of the [chartValuePairs].
  final int xAxisStepCount;

  /// Basically the amount of labels that will be displayed between the minimun and the maximum **Y** values of the [chartValuePairs].
  final int yAxisStepCount;
  final bool drawPath;
  final bool drawDots;
  final bool drawGrid;
  final bool showAverageLevel;
  final bool showLabels;
  final bool drawBorders;
  final bool drawHighlights;
  final bool useDragInteraction;
  final ChartPathStyle pathStyle;
  final ChartBorderStyle borderStyle;
  final ChartLineStyle gridLineStyle;
  final ChartLineStyle averageLevelLineStyle;
  final ChartPointStyle pointStyle;
  final TextStyle labelStyle;

  final bool horizontalDragActive;
  final double horizontalDragPosition;

  /// The left offset of the chart.
  /// This is used to draw labels in front of the **Y** axis.
  final double xOffset;
  final double yOffset;
  _LineChartPainter({
    @required this.chartValues,
    this.animationValue,
    this.xAxisStepCount,
    this.yAxisStepCount,
    this.drawPath = true,
    this.pathStyle,
    this.drawDots = true,
    this.drawGrid = false,
    this.showAverageLevel = false,
    this.showLabels = true,
    this.drawBorders = true,
    this.useDragInteraction,
    this.borderStyle,
    this.gridLineStyle,
    this.averageLevelLineStyle,
    this.pointStyle,
    this.labelStyle,
    this.horizontalDragActive = false,
    this.horizontalDragPosition,
    this.drawHighlights = false,
    this.xOffset = 40,
    this.yOffset = 20,
  });

  /// The space between two labels on the **X** axis.
  // double _xAxisStepSize;

  /// The space between two labels on the **Y** axis.
  double _yAxisStepSize;

  /// The list of labels to be displayed on the **X** axis.
  List<AxisText> _xAxisTexts = [];

  /// The list of labels to be displayed on the **Y** axis.
  List<AxisText> _yAxisTexts = [];

  /// The array of TextPainters which will be rendered on the bottom of the **X** axis.
  List<TextPainter> _xTexts = [];

  /// The array of TextPainters which will be rendered on the side of the **Y** axis.
  List<TextPainter> _yTexts = [];

  void _drawBorders(Canvas canvas, Size size, Paint paint) {
    Path path = Path()
      ..moveTo(xOffset, 0)
      ..lineTo(xOffset, size.height - yOffset)
      ..lineTo(size.width, size.height - yOffset);

    canvas.drawPath(path, paint);
  }

  void _drawPath(Canvas canvas, Size size) {
    Paint pathStroke = Paint()
      ..color = pathStyle.pathColor
      ..strokeWidth = pathStyle.pathWidth
      ..style = PaintingStyle.stroke
      ..strokeMiterLimit = 0
      ..strokeJoin = StrokeJoin.miter;

    Path path = Path();
    bool init = true;
    chartValues.chartValuePairs.forEach((point) {
      if (init) {
        init = false;
        path.moveTo(canvasPoint(point, size).dx, canvasPoint(point, size).dy);
      }
      path.lineTo(canvasPoint(point, size).dx, canvasPoint(point, size).dy);
    });
    Path animatedPath = Path();
    if (animationValue < 1.0) {
      animatedPath = AnimatedPathUtil.createAnimatedPath(path, animationValue);
    } else {
      animatedPath = path;
    }
    canvas.drawPath(animatedPath, pathStroke);
  }

  void _fillPath(Canvas canvas, Size size) {
    final Gradient gradient = LinearGradient(
      colors: <Color>[
        pathStyle.pathFillColor.withOpacity(0.0),
        pathStyle.pathFillColor.withOpacity(animationValue * 0.8),
      ],
      begin: Alignment.bottomCenter,
      end: Alignment.topCenter,
    );
    Rect rect = Rect.fromLTWH(yOffset, 0, size.width - yOffset, size.height);
    Paint pathFill = Paint()
      ..color = pathStyle.pathFillColor
      ..style = PaintingStyle.fill
      ..shader = gradient.createShader(rect);
    Path path = Path()
      ..moveTo(
        xOffset,
        size.height - yOffset,
      );
    chartValues.chartValuePairs.forEach((point) {
      path.lineTo(canvasPoint(point, size).dx, canvasPoint(point, size).dy);
    });
    path.lineTo(size.width, size.height - yOffset);

    canvas.drawPath(path, pathFill);
  }

  void _drawDots(Canvas canvas, Size size) {
    Paint paintFill = Paint()..color = pointStyle.color;
    Paint paintStroke = Paint()
      ..strokeWidth = pointStyle.strokeWidth
      ..color = pointStyle.strokeColor;

    chartValues.chartValuePairs.forEach((point) {
      canvas.drawCircle(canvasPoint(point, size), pointStyle.radius, paintFill);

      if (pointStyle.useStroke) {
        canvas.drawCircle(
            canvasPoint(point, size), pointStyle.strokeWidth, paintStroke);
      }
    });
  }

  void _drawAverageLevel(Canvas canvas, Size size) {
    double allTogether = 0;
    chartValues.chartValuePairs.forEach((point) {
      allTogether += point.y * -size.height + size.height;
    });
    Paint paint = Paint()
      ..strokeWidth = averageLevelLineStyle.width
      ..color = averageLevelLineStyle.color;
    canvas.drawLine(
        Offset(0, allTogether / chartValues.chartValuePairs.length),
        Offset(size.width, allTogether / chartValues.chartValuePairs.length),
        paint);
  }

  void _drawLabels(Canvas canvas, Size size, Paint borderPaint) {
    for (int i = 0; i < _xTexts.length; i++) {
      Offset textPosition = Offset(
        _xAxisTexts[i].position - _xTexts[i].width / 2,
        size.height - yOffset + 5,
      );
      _xTexts[i].paint(canvas, textPosition);
      canvas.drawLine(
        Offset(_xAxisTexts[i].position, size.height - yOffset + 5),
        Offset(_xAxisTexts[i].position, size.height - yOffset),
        borderPaint,
      );
    }

    for (int i = 0; i < _yTexts.length; i++) {
      Offset textPosition = Offset(
        xOffset - _yTexts[i].width - 5,
        _yAxisTexts[i].position - yOffset - _yTexts[i].height / 2,
      );
      _yTexts[i].paint(canvas, textPosition);
      canvas.drawLine(
        Offset(xOffset, _yAxisTexts[i].position - yOffset),
        Offset(xOffset - 5, _yAxisTexts[i].position - yOffset),
        borderPaint,
      );
    }
  }

  void _drawGrid(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..strokeWidth = gridLineStyle.width
      ..color = gridLineStyle.color;

    for (int i = 0; i < _xTexts.length; i++) {
      double positionX = _xAxisTexts[i].position;

      canvas.drawLine(
        Offset(positionX, 0),
        Offset(positionX, size.height - yOffset),
        paint,
      );
    }

    for (int i = 0; i < _yTexts.length; i++) {
      double positionY = _yAxisTexts[i].position - yOffset;
      canvas.drawLine(
        Offset(xOffset, positionY),
        Offset(size.width, positionY),
        paint,
      );
    }
  }

  void _drawHighlight(Canvas canvas, Size size) {
    if (animationValue < 1) return;
    Paint paint = Paint()
      ..strokeWidth = averageLevelLineStyle.width
      ..color = averageLevelLineStyle.color;

    for (int index = 0; index < chartValues.minMaxY.length; index++) {
      final textValue = convertNumberToAxisLabel(
        number: chartValues.minMaxY[index].y,
        stepSize: ((ChartValuePair.convertToDouble(chartValues.minMaxY[1].y) -
                ChartValuePair.convertToDouble(chartValues.minMaxY[0].y)) /
            yAxisStepCount),
      );
      TextSpan span = TextSpan(
        style: labelStyle,
        text: textValue,
      );
      TextPainter tp = TextPainter(
          text: span,
          textAlign: TextAlign.left,
          textDirection: TextDirection.ltr);
      tp.layout();
      Offset position;

      if (index == 0) {
        Offset cp = canvasPoint(chartValues.minMaxY[0], size);
        position = Offset(
          cp.dx + 12,
          cp.dy + tp.height,
        );
        if (cp.dx > size.width - 50) {
          position = Offset(cp.dx - (2 * 12), cp.dy + tp.height);
        }

        canvas.drawLine(cp, position, paint);
      } else {
        Offset cp = canvasPoint(chartValues.minMaxY[1], size);

        position = Offset(
          cp.dx + 12,
          cp.dy - tp.height * 2,
        );
        if (cp.dx > size.width - 50) {
          position = Offset(
            cp.dx - (2 * 12),
            cp.dy - tp.height * 2,
          );
        }

        canvas.drawLine(cp, Offset(position.dx, cp.dy - tp.height), paint);
      }
      tp.paint(canvas, position);
    }
  }

  void _handleDragInteraction(Canvas canvas, Size size) {
    if (horizontalDragActive &&
        horizontalDragPosition > xOffset &&
        horizontalDragPosition < size.width) {
      final double xLinePos = horizontalDragPosition;

      Paint paint = Paint()..color = Colors.green;

      final valuePair = getChartValuePairByXAxis(
          xLinePos, canvas, Size(size.width, size.height));
      final convertedPair = canvasPoint(valuePair, size);

      canvas.drawLine(
        Offset(0, convertedPair.dy),
        Offset(size.width, convertedPair.dy),
        paint,
      );

      canvas.drawLine(
        Offset(convertedPair.dx, 0),
        Offset(convertedPair.dx, size.height),
        paint,
      );

      String textX = valuePair.x.toString();
      if (valuePair.x is DateTime) {
        final DateTime time = valuePair.x;
        textX = formatDateTime("dd.MM. HH:mm", time);
      }

      TextSpan span = TextSpan(
        style: labelStyle ??
            TextStyle(
              color: Colors.black,
              fontSize: 12,
            ),
        text: "$textX\n${valuePair.y}",
      );
      TextPainter tp = TextPainter(
          text: span,
          textAlign: TextAlign.left,
          textDirection: TextDirection.ltr);
      tp.layout();
      tp.paint(canvas, Offset(xLinePos, 0));
    }
  }

  /// Convert a ChartValuePair into a canvas scaled offset.
  /// [point] - The point to convert.
  /// [size] - The size of the canvas.
  Offset canvasPoint(
    ChartValuePair point,
    Size size,
  ) {
    final rawX = chartValues.minMaxX;
    final rawY = chartValues.minMaxY;

    final position = ChartValuePair.convertToOffset(
      point: point,
      minPoint: ChartValuePair(x: rawX[0].x, y: rawY[0].y),
      maxPoint: ChartValuePair(x: rawX[1].x, y: rawY[1].y),
      newMinSize: Size(xOffset, _yAxisStepSize),
      newMaxSize: Size(size.width, size.height - _yAxisStepSize),
    );
    return Offset(position.dx, size.height - yOffset - position.dy);
  }

  void initialize(Size size) {
    chartValues.initialize();

    final axes = [Axis.horizontal, Axis.vertical];
    axes.forEach((axis) {
      final minMax =
          axis == Axis.horizontal ? chartValues.minMaxX : chartValues.minMaxY;

      switch (minMax[0].getAxis(axis).runtimeType) {
        // Calculate axes if minMax[0].getAxis(axis) is of type DateTime
        case DateTime:
          Duration timeSpan =
              minMax[1].getAxis(axis).difference(minMax[0].getAxis(axis));
          int stepCount;
          if (axis == Axis.horizontal) {
            stepCount = xAxisStepCount;
          } else {
            stepCount = yAxisStepCount;
          }

          double stepSize;
          if (axis == Axis.horizontal) {
            stepSize = ((size.width - xOffset) / stepCount);
            // _xAxisStepSize = stepSize;
          } else {
            stepSize = (size.height / stepCount);
            _yAxisStepSize = stepSize;
          }

          double stepInSeconds = timeSpan.inSeconds.roundToDouble() / stepCount;

          for (int i = 0; i < stepCount + 1; i++) {
            DateTime tick = minMax[0]
                .getAxis(axis)
                .add(Duration(seconds: (stepInSeconds * i).round()));
            final position = (i * stepSize);
            String labelText = convertDateTimeToAxisLabel(
              time: tick,
              stepDuration: Duration(seconds: stepInSeconds.round()),
            );

            axis == Axis.horizontal
                ? _xAxisTexts.add(
                    AxisText(text: labelText, position: position + xOffset))
                : _yAxisTexts
                    .add(AxisText(text: labelText, position: position));

            TextSpan span = TextSpan(
              style: labelStyle ??
                  TextStyle(
                    color: Colors.black,
                    fontSize: 12,
                  ),
              text: labelText,
            );
            TextPainter tp = TextPainter(
              text: span,
              textAlign:
                  axis == Axis.horizontal ? TextAlign.center : TextAlign.right,
              textDirection: TextDirection.ltr,
            );
            tp.layout();

            axis == Axis.horizontal ? _xTexts.add(tp) : _yTexts.add(tp);
          }
          break;
        // Calculate axes if minMax[0].getAxis(axis) is of type double or else.
        case double:
        default:
          int stepCount;
          double stepSize;
          if (axis == Axis.horizontal) {
            stepCount = xAxisStepCount;
            stepSize = size.width / stepCount;
            // _xAxisStepSize = stepSize;
          } else {
            stepCount = yAxisStepCount;
            stepSize = size.height / (stepCount + 2);
            _yAxisStepSize = stepSize;
          }

          double low = minMax[0].getAxis(axis);
          double high = minMax[1].getAxis(axis);

          double valueStepSize = ((high - low) / stepCount);

          for (int i = 0; i <= stepCount + 1; i++) {
            double labelValue;
            if (axis == Axis.horizontal) {
              labelValue = low + (i * valueStepSize);
            } else {
              labelValue = low - valueStepSize + (i * valueStepSize);
            }

            String labelText;
            labelText = convertNumberToAxisLabel(
                number: labelValue, stepSize: valueStepSize);

            double position;
            if (axis == Axis.horizontal) {
              position = (i * stepSize);
              _xAxisTexts.add(AxisText(text: labelText, position: position));
            } else {
              position = size.height - (i * stepSize);
              _yAxisTexts.add(AxisText(text: labelText, position: position));
            }

            TextSpan span = TextSpan(
              style: labelStyle ??
                  TextStyle(
                    color: Colors.black,
                    fontSize: 12,
                  ),
              text: labelText,
            );
            TextPainter tp = TextPainter(
              text: span,
              textAlign:
                  axis == Axis.horizontal ? TextAlign.center : TextAlign.right,
              textDirection: TextDirection.ltr,
            );
            tp.layout();

            axis == Axis.horizontal ? _xTexts.add(tp) : _yTexts.add(tp);
          }
          break;
      }
    });
  }

  /// Get the [ChartValuePair] on a specific point of the **X** axis.
  ChartValuePair getChartValuePairByXAxis(
      double xPositionOnCanvas, Canvas canvas, Size size) {
    ChartValuePair nearestLowest = chartValues.minMaxX[0];

    ChartValuePair nearestHighest = chartValues.minMaxX[1];
    chartValues.chartValuePairs.forEach((pointRaw) {
      final point = canvasPoint(pointRaw, size);
      if (point.dx > canvasPoint(nearestLowest, size).dx &&
          point.dx < xPositionOnCanvas) {
        nearestLowest = pointRaw;
      }
      if (point.dx < canvasPoint(nearestHighest, size).dx &&
          point.dx > xPositionOnCanvas) {
        nearestHighest = pointRaw;
      }
    });
    final nearestHighestY = ChartValuePair.convertToDouble(nearestHighest.y);
    final nearestLowestY = ChartValuePair.convertToDouble(nearestLowest.y);
    final nearestHighestX = ChartValuePair.convertToDouble(nearestHighest.x);
    final nearestLowestX = ChartValuePair.convertToDouble(nearestLowest.x);

    double m =
        (nearestHighestY - nearestLowestY) / (nearestHighestX - nearestLowestX);
    double b = nearestHighestY - (m * nearestHighestX);
    final rawX = convertToNewRange(
      point: xPositionOnCanvas,
      oldMin: xOffset,
      oldMax: size.width,
      newMin: ChartValuePair.convertToDouble(chartValues.minMaxX[0].x),
      newMax: ChartValuePair.convertToDouble(chartValues.minMaxX[1].x),
    );
    final yCoords = m * rawX + b;
    final xCoords = rawX;

    dynamic x = xCoords;
    dynamic y = yCoords;
    if (nearestLowest.x is DateTime) {
      x = DateTime.fromMillisecondsSinceEpoch(xCoords.floor());
    }
    if (nearestLowest.y is DateTime) {
      y = DateTime.fromMillisecondsSinceEpoch(yCoords.floor());
    }

    return ChartValuePair(
      x: x,
      y: y,
    );
  }

  @override
  void paint(Canvas canvas, Size size) {
    initialize(size);

    Paint borderPaint = Paint()
      ..color = borderStyle.color
      ..strokeWidth = borderStyle.width
      ..style = PaintingStyle.stroke;
    if (showLabels) _drawLabels(canvas, size, borderPaint);
    if (drawHighlights) _drawHighlight(canvas, size);
    if (useDragInteraction) _handleDragInteraction(canvas, size);
    if (drawBorders) _drawBorders(canvas, size, borderPaint);
    if (drawPath) _drawPath(canvas, size);
    if (drawDots) _drawDots(canvas, size);
    if (pathStyle.fillPath) _fillPath(canvas, size);
    if (showAverageLevel) _drawAverageLevel(canvas, size);
    if (drawGrid) _drawGrid(canvas, size);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
