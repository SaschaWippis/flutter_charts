import 'package:flutter/material.dart';

class CircleChartValue {
  String label;
  double value;
  Color color;

  CircleChartValue({
    @required this.label,
    @required this.value,
    this.color = Colors.white,
  });
}
