import 'dart:math';

class CircleArcElement {
  double degree;
  double startDegree;

  CircleArcElement({
    this.degree,
    this.startDegree,
  });

  double get radian => degree * (pi / 180);
  double get startRadian => startDegree * (pi / 180);
}
