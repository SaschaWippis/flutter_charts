import 'dart:math';

import 'package:charts/charts/circular_chart/models/circle_arc_element.dart';
import 'package:flutter/material.dart';
import 'package:charts/charts/circular_chart/models/circle_chart_value.dart';

class CircularChart extends StatefulWidget {
  final List<CircleChartValue> data;
  final bool sort;

  /// Defaults to full circle (360)
  final double maximumDegrees;

  /// -90 equals 12 o'clock
  final double startAtDegrees;

  /// Size of the inner circle. 0 to 1
  final double innerCircleRadiusPercentage;
  final double circleRadiusPercentage;

  final String valueSuffix;
  final TextStyle labelStyle;
  final Widget child;
  final BoxShadow shadow;
  final bool animate;
  final Duration animationDuration;
  const CircularChart({
    Key key,
    @required this.data,
    this.sort = true,
    this.maximumDegrees = 360,
    this.startAtDegrees = -90,
    this.innerCircleRadiusPercentage = 0.5,
    this.circleRadiusPercentage = 1,
    this.valueSuffix = "",
    this.labelStyle,
    this.child,
    this.shadow,
    this.animate,
    this.animationDuration = const Duration(milliseconds: 400),
  }) : super(key: key);

  @override
  _CircularChartState createState() => _CircularChartState();
}

class _CircularChartState extends State<CircularChart>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: widget.animationDuration,
    )
      ..fling(
        velocity: 1,
        animationBehavior: AnimationBehavior.normal,
      )
      ..forward();
    _animationController.addListener(() {
      setState(() {});
    });
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return AspectRatio(
          aspectRatio: 1 / 1,
          child: Container(
            child: CustomPaint(
              painter: CircularChartPainter(
                data: widget.data,
                sort: widget.sort,
                maximumDegrees: widget.maximumDegrees,
                startAtDegrees: widget.startAtDegrees,
                innerCircleRadiusPercentage: widget.innerCircleRadiusPercentage,
                circleRadiusPercentage: widget.circleRadiusPercentage,
                valueSuffix: widget.valueSuffix,
                labelStyle: widget.labelStyle,
                animationValue: _animationController.value,
              ),
              child: widget.child,
            ),
          ),
        );
      },
    );
  }
}

class CircularChartPainter extends CustomPainter {
  final List<CircleChartValue> data;
  final bool sort;

  /// Defaults to full circle (360)
  final double maximumDegrees;

  /// -90 equals 12 o'clock
  final double startAtDegrees;

  /// Size of the inner circle. 0 to 1
  final double innerCircleRadiusPercentage;
  final double circleRadiusPercentage;
  final String valueSuffix;
  final TextStyle labelStyle;
  final double animationValue;

  final BoxShadow shadow;
  CircularChartPainter({
    @required this.data,
    this.sort,
    this.maximumDegrees = 360,
    this.startAtDegrees = -90,
    this.innerCircleRadiusPercentage = 0.5,
    this.circleRadiusPercentage = 1,
    this.valueSuffix,
    this.labelStyle,
    this.animationValue,
    this.shadow,
  });

  List<CircleArcElement> _elements = [];
  double _totalValue = 0;
  double _circleRadius;
  double _innerCircleRadius;
  double _center;

  void _drawChart(Canvas canvas, Size size) {
    for (int i = 0; i < data.length; i++) {
      final degree = data[i].value * maximumDegrees / _totalValue;

      if (i == 0) {
        _elements
            .add(CircleArcElement(degree: degree, startDegree: startAtDegrees));
      } else {
        _elements.add(CircleArcElement(
            degree: degree,
            startDegree:
                _elements[i - 1].startDegree + _elements[i - 1].degree));
      }
    }

    final innerRect = Rect.fromCircle(
        center: Offset(_center, _center), radius: _innerCircleRadius);
    final outerRect = Rect.fromCircle(
        center: Offset(_center, _center), radius: _circleRadius);

    for (int i = 0; i < data.length; i++) {
      final element = _elements[i];

      Path path = Path()
        ..moveTo(_center + _innerCircleRadius * cos(element.startRadian),
            _center + _innerCircleRadius * sin(element.startRadian))
        ..lineTo(
            _center + _circleRadius * cos(animationValue * element.startRadian),
            _center + _circleRadius * sin(animationValue * element.startRadian))
        ..arcTo(
          outerRect,
          animationValue * element.startRadian,
          animationValue * element.radian,
          true,
        )
        ..lineTo(
            _center +
                _innerCircleRadius *
                    cos(animationValue *
                        (element.startRadian + element.radian)),
            _center +
                _innerCircleRadius *
                    sin(animationValue *
                        (element.startRadian + element.radian)))
        ..arcTo(
          innerRect,
          animationValue * (element.startRadian + element.radian),
          -(animationValue * element.radian),
          false,
        )
        ..close();

      Paint paintFill = Paint()
        ..color = data[i].color
        ..style = PaintingStyle.fill;
      if (shadow != null) canvas.drawPath(path, shadow.toPaint());
      canvas.drawPath(path, paintFill);
    }
  }

  void _drawLabels(Canvas canvas, Size size) {
    for (int i = 0; i < _elements.length; i++) {
      if (i > data.length - 1) continue;
      final element = _elements[i];
      TextSpan span = TextSpan(
        style: TextStyle(
          color: data[i].color,
          fontSize: 14,
        ),
        text: data[i].value.toStringAsFixed(2) + valueSuffix,
      );
      TextPainter tp = TextPainter(
        text: span,
        textAlign: TextAlign.center,
        textDirection: TextDirection.ltr,
      );
      tp.layout();

      final radius = (circleRadiusPercentage + 0.1) * (size.width / 2);
      final baseOffset = Offset(
          _center + radius * cos(element.startRadian + element.radian / 2),
          _center + radius * sin(element.startRadian + element.radian / 2));

      tp.paint(canvas,
          Offset(baseOffset.dx - tp.width / 2, baseOffset.dy - tp.height / 2));
    }
  }

  @override
  void paint(Canvas canvas, Size size) {
    if (sort) data.sort((a, b) => b.value.compareTo(a.value));
    data.forEach((e) => _totalValue += e.value);

    _innerCircleRadius = innerCircleRadiusPercentage * (size.width / 2);
    _circleRadius = circleRadiusPercentage * (size.width / 2);
    _center = size.width / 2;
    _drawChart(canvas, size);
    _drawLabels(canvas, size);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
