import 'package:flutter/material.dart';

class ChartLineStyle {
  final double width;
  final Color color;

  const ChartLineStyle({
    this.width = 1,
    this.color = Colors.black45,
  });
}
