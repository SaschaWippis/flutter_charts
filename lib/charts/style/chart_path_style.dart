import 'package:flutter/material.dart';

class ChartPathStyle {
  final Color pathColor;
  final double pathWidth;
  final bool fillPath;
  final Color pathFillColor;

  const ChartPathStyle({
    this.pathColor = Colors.black,
    this.pathWidth = 2,
    this.fillPath = false,
    this.pathFillColor = Colors.black26,
  });
}
