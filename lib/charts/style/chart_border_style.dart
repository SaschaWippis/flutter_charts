import 'package:flutter/material.dart';

class ChartBorderStyle {
  final double width;
  final Color color;

  const ChartBorderStyle({
    this.width = 1,
    this.color = Colors.black,
  });
}
