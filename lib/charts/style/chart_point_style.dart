import 'package:flutter/material.dart';

class ChartPointStyle {
  final Color color;
  final double radius;
  final bool useStroke;
  final Color strokeColor;
  final double strokeWidth;

  const ChartPointStyle({
    this.color = Colors.black,
    this.radius = 3,
    this.useStroke = false,
    this.strokeColor = Colors.black54,
    this.strokeWidth = 1,
  });
}
