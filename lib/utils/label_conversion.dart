import 'package:flutter/material.dart';

String convertNumberToAxisLabel({
  @required double number,
  @required double stepSize,
}) {
  if (stepSize < 0.1) {
    return number.toStringAsFixed(3);
  } else if (stepSize < 1) {
    return number.toStringAsFixed(2);
  } else if (stepSize < 5) {
    return number.toStringAsFixed(1);
  } else {
    return number.round().toString();
  }
}

String convertDateTimeToAxisLabel({
  @required DateTime time,
  @required Duration stepDuration,
}) {
  if (stepDuration.inHours < 12) {
    return "${time.hour.toString().padLeft(2, '0')}:${time.minute.toString().padLeft(2, '0')}";
  }
  return "${time.day.toString().padLeft(2, '0')}.${time.month.toString().padLeft(2, '0')}.";
}
