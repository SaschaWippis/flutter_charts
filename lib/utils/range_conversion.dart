double convertToNewRange({
  double point,
  double oldMin,
  double oldMax,
  double newMin,
  double newMax,
}) {
  final double oldRange = (oldMax - oldMin);
  final double newRange = (newMax - newMin);
  return (((point - oldMin) * newRange) / oldRange) + newMin;
}
