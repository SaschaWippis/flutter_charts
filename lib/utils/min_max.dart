List<double> minMaxed(List<double> values) {
  double min;
  double max;

  values.forEach((e) {
    if (min == null) {
      min = e;
      max = e;
    } else {
      if (e < min) {
        min = e;
      }
      if (e > max) {
        max = e;
      }
    }
  });

  return [min, max];
}
