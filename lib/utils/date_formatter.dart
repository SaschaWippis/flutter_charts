String formatDateTime(String dateFormat, DateTime time) {
  String formatted = "";
  formatted = dateFormat
      .replaceAll("MM", padTime(time.month))
      .replaceAll("yyyy", time.year.toString())
      .replaceAll("dd", padTime(time.day))
      .replaceAll("HH", padTime(time.hour))
      .replaceAll("mm", padTime(time.minute))
      .replaceAll("ss", padTime(time.second));

  return formatted;
}

String padTime(int time, {int width = 2}) {
  return time.toString().padLeft(2, '0');
}
